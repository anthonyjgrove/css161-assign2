
/**
 * This program sorts and prints the sides of a triangle given to use by the user 
 * in order from smallest value to largest value and then print the type of triangle.
 * 
 * @author Anthony Grove 
 * @version CSS161 - Assign2 
 */
import java.util.Scanner;

public class Assign2
{
    public static void main(String[] args)
    {

        //triangle legnth inputs
        Scanner triangle = new Scanner(System.in);
        System.out.print("Enter three numbers to form a triangle: ");

        int a, b, c, x;

        a = triangle.nextInt();

        b = triangle.nextInt();

        c = triangle.nextInt();

        //This section will arrange and print the sides of the triangle from smallest to largest 
        if (b < a)
        {
            x = a;
            a = b;
            b = x;
        }

        if (c < b) 
        {
            x = c;
            c = b;
            b = x;
        }

        if (b < a)
        {
            x = a;
            a = b;
            b = x;
        }

        System.out.println("Triangle sides: " + " " + a + " " + b + " " + c);        
        //Print type of triangle        
        //Does not form triangle -- invalid lengths
        if (a >= (b+c) || c >= (a+b) || (c <= 0)) 
        {
            System.out.println("Triangle kind: DOES NOT FORM TRIANGLE!");
        }
        //Equilateral -- all sides are equal length
        else if ((a == b) && (b == c))
        {
            System.out.println("Triangle kind: Equilateral!");
        }
        //Isosceles -- only two sides are of equal length
        else if ((a == b) || (b == c))
        {
            System.out.println("Triangle kind: Isosceles!");
        }
        //Scalene -- all three sides are different lengths
        else if ((a != b) && (b != c))
        {
            System.out.println("Triangle kind: Scalene!");
        }

        else
            System.out.println("Triangle kind: DOES NOT FORM TRIANGLE!");
    }

}

